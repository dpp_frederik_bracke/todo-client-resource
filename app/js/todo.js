(function () {
    "use strict";

    var todoApp = angular.module('todoApp', ['ngRoute','ngResource']);

    todoApp.factory('TodoItem', function($resource){
        var ROOT = 'https://buoyant-sum-777.appspot.com/_ah/api/todo/v1/';
        return $resource(ROOT, {}, {
            query: {url: ROOT + 'todocollection' ,method: 'GET', isArray: true, transformResponse: function(data, header){
                var wraped = angular.fromJson(data);
                return wraped.items;
            }},
            get: {url: ROOT + 'todo/:id', method: 'GET'},
            save: {url: ROOT + 'saveTodo', method: 'POST'}
        });
    });

    todoApp.config(function($routeProvider) {
        $routeProvider.when('/', { templateUrl: 'partials/todo-list.html', controller: 'todoController'})
            .when('/todo-detail/:detailId', { templateUrl: 'partials/todo-detail.html', controller: 'todoDetailController'})
            .when('/todo-create', { templateUrl: 'partials/todo-create.html', controller: 'todoCreateController'})
            .otherwise({ redirectTo: '/'});
    });


    todoApp.controller('todoDetailController', function ($scope, $routeParams, TodoItem){
        $scope.todo = TodoItem.get({id: $routeParams.detailId});
    });

    todoApp.controller('todoCreateController', function ($scope, TodoItem) {
        $scope.success=false;
        $scope.error=false;

        $scope.createNew = function(){
            var todo = $scope.todo;
            todo.done = false;
            TodoItem.save(todo,
            function (){
                $scope.success=true;
            },
            function(error){
                $scope.error=true;
                $scope.errorMessage = error.data.message;
            });
            $scope.todo = {};
        };

        $scope.clearForm = function(){
            $scope.todo = {};
        };
    });

    todoApp.controller('todoController', function ($scope, TodoItem) {
        var todoItems = TodoItem.query(function(){
            $scope.todoItems = todoItems;
        });


        $scope.remaining = function() {
            var count = 0;
            angular.forEach($scope.todoItems, function(todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };
    });

    todoApp.filter('prettyPrint', function () {
        return function (text) {
            if(text) {
                console.log(text);
                return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
            }
        };
    });

    todoApp.directive('todo', function () {
        return {
            restrict: 'E',
            scope: {
                link: '@url',
                todoItem: '=item'
            },
            template: '<input type="checkbox" ng-model="todoItem.done"> <a href="{{ link }}"><span class="done-{{ todoItem.done }}">{{ todoItem.name | prettyPrint}}</span></a>'
        } ;
    });
}());
